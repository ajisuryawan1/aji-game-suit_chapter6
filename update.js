const { Article } = require("./models");

const query = {
    where: { id: 1 }
}

Article.update({
    approved: false
}, query)
    .then(() => {
        console.log("artikel berhasil diupdate");
        process.exit();
    })
    .catch(err => {
        console.log("gagal mengupdate artikel");
    });