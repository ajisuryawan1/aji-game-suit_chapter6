const { Article } = require("./models");

Article.create({
    title: "Hello World",
    body: "Lorem ipsum",
    approved: true
})
    .then(article => {
        console.log(article);
    });