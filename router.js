const express = require("express");
const router = express.Router();
const user = require("./user.json");
const { User } = require("./models");
const { UserBiodata } = require("./models");
const { Article } = require("./models");

router.get("/", (req, res) => {
    res.render("index");
});

router.get("/game", (req, res) => {
    res.render("game");
});

router.get("/login", (req, res) => {
    res.render("login");
});

router.post("/login", (req, res) => {
    const { email, password } = req.body;

    if (user.email === email && user.password === password) {
        res.redirect("/game");
    } else {
        res.redirect("/login");
    }
});

router.get("/articles/create", (req, res) => {
    res.render("articles/create")
});

router.get("/articles/:id/edit", (req, res) => {
    Article.findOne({
        where: { id: req.params.id }
    })
        .then(article => {
            res.render("articles/edit", { 
                article 
            });
        });
});

router.get("/articles", (req, res) => {
    Article.findAll()
        .then(articles => {
            res.render("articles/index", {
                articles
            });
        });
});

router.get("/articles/:id", (req, res) => {
    Article.findOne({
        where: { id: req.params.id }
    })
        .then(article => {
            res.render("articles/show", { 
                article 
            });
        });
});

router.post("/articles", (req, res) => {
    Article.create({
        title: req.body.title,
        body: req.body.body,
        approved: req.body.approved
    })
        .then(article => {
            res.send("article berhasil dibuat");
        })
        .catch(err => {
            res.send("can't create articles");
        });
});

router.post("/articles/:id", (req, res) => {
    Article.update({
        title: req.body.title,
        body: req.body.body,
        approved: req.body.approved
    }, {
        where: { id: req.params.id }
    })
        .then(article => {
            res.send("article berhasil diupdate");
        })
        .catch(err => {
            res.send("can't update articles");
        });
});

router.post("/articles/:id/delete", (req, res) => {
    Article.destroy({
        where: { id: req.params.id }
    })
        .then(article => {
            res.send("article berhasil dihapus");
        })
        .catch(err => {
            res.send("can't delete articles");
        });
});

router.get("/users/create", (req, res) => {
    res.render("users/create");
});

router.post("/users", (req, res) => {
    User.create({
        username: req.body.username,
        password: req.body.password
    })
        .then(user => {
            UserBiodata.create({
                user_game_id: user.id,
                name: req.body.name,
                phone: req.body.phone,
                address: req.body.address
            })
                .then(()  => {
                    res.send("user berhasil dibuat")
                })
                .catch(err => {
                    console.log(err);
                    res.send("gagal buat user biodata");
                })
        })
        .catch(err => {
            console.log(err);
            res.send("gagal buat user");
        });
});

router.get("/users", (req, res) => {
    UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
    User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });

    User.findAll({
        include: [{
            model: UserBiodata
           }]
    })
        .then(users => {
            console.log(users);
            res.render("users/index", {
                users
            });
        });
});

router.get("/users/:id", (req, res) => {
    UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
    User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });

    User.findOne({
        where: { id: req.params.id },
        include: [{
            model: UserBiodata
           }]
    })
        .then(user => {
            console.log(user);
            res.render("users/show", { 
                user
            });
        });
});

router.get("/users/:id/edit", (req, res) => {
    UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
    User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });

    User.findOne({
        where: { id: req.params.id },
        include: [{
            model: UserBiodata
           }]
    })
        .then(user => {
            console.log(user);
            res.render("users/edit", { 
                user
            });
        });
});

router.post("/users/:id", (req, res) => {
    User.update({
        username: req.body.username
    }, {
        where: { id: req.params.id }
    })
        .then(user => {
            UserBiodata.update({
                name: req.body.name,
                phone: req.body.phone,
                address: req.body.address
            }, {
                where: { user_game_id: req.params.id }
            })
                .then(()  => {
                    res.send("user berhasil diupdate")
                })
                .catch(err => {
                    console.log(err);
                    res.send("gagal update user biodata");
                })
        })
        .catch(err => {
            console.log(err);
            res.send("gagal update user");
        });
});

router.post("/users/:id/delete", (req, res) => {
    User.destroy({
        where: { id: req.params.id }
    })
        .then(user => {
            UserBiodata.destroy({
                where: { user_game_id: req.params.id }
            })
                .then(()  => {
                    res.send("user berhasil dihapus")
                })
                .catch(err => {
                    console.log(err);
                    res.send("gagal hapus user biodata");
                })
        })
        .catch(err => {
            console.log(err);
            res.send("gagal hapus user");
        });
});

module.exports = router;
