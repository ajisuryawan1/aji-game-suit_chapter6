const express = require("express");
const app = express();
const port = 8000;
const router = require("./router");

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
}

app.use(logger);
app.use(express.urlencoded({ extended: false }));
app.use(router);
app.use(express.static(`${__dirname}/assets`));
app.set("view engine", "ejs");

// internal server error handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: "fail",
        errors: err.message,
    });
});

// 404 handler
app.use((req, res, next) => {
    res.status(404).json({
        status: "fail",
        errors: "are you lost?",
    });
});

app.listen(port, () => console.log(`server nyala http://localhost:${port}`));
