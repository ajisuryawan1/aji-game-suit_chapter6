const luasSegitiga = (alas, tinggi) => alas * tinggi / 2;

module.exports = luasSegitiga;